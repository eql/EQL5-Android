
### Info

Example `tic-tac-toe/` has the most detailed description.

**ASDF** integration can be found in `REPL/` and `my/`. Those 2 examples are
also the only ones prepared for both **32bit** and **64bit** (all others are
64bit only).

A basic app skeleton, but containing all EQL5 modules plus many QML modules
for playing around, and including a simple REPL (hidden by default), can be
found in `my/`.

The `my` example is also the only one describing how to do **i18n**.
