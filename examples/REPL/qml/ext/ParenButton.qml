import QtQuick 2.13
import QtQuick.Controls 2.13

Button {
  id: parenButton
  width: main.isPhone ? 35 : 55
  height: width
  focusPolicy: Qt.NoFocus
  flat: true
  opacity: 0.12

  property url source

  background: Image {
    source: parent.source
  }
}
