import QtQuick 2.10
import QtQuick.Controls 2.5
import "ext/" as Ext
import EQL5 1.0

Rectangle {
    id: main
    width: 210  // for desktop
    height: 210 // (see above)
    color: "#707070"

    SwipeView {
        id: view
        anchors.fill: parent
        orientation: Qt.Vertical
        currentIndex: 1

        // calculator extended (page 1)

        Item {
            Repeater {
                model: 12

                Ext.InputButton {
                    x: Lisp.call("watch:button-pos", "x", index, main.width - height, "extended")
                    y: Lisp.call("watch:button-pos", "y", index, main.width - height, "extended")
                    text: Lisp.call("watch:button-text", index, "extended")

                    background: Rectangle {
                        radius: width / 2
                        color: parent.pressed ? "white" : "transparent"
                    }

                    contentItem: Text {
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font.pixelSize: 14
                        font.bold: true
                        color: "white"
                        text: parent.text
                    }
                }
            }

            // blah button

            Ext.InputButton {
                objectName: "blah"
                anchors.horizontalCenter: parent.horizontalCenter
                y: (main.width - height) / 2 + 68
                width: 65
                height: 28
                font.bold: false
                text: "blah"
                visible: (view.currentIndex == 0)

                background: Rectangle {
                    radius: width / 2
                    color: parent.pressed ? "darkgray" : "lightgray"
                }
            }
        }

        // calculator main (page 2 -- default)

        Item {
            Repeater {
                model: 14

                Ext.InputButton {
                    x: Lisp.call("watch:button-pos", "x", index, main.width - height)
                    y: Lisp.call("watch:button-pos", "y", index, main.width - height)
                    text: Lisp.call("watch:button-text", index)

                    background: Rectangle {
                        radius: width / 2
                        color: parent.pressed ? "darkgray" : Lisp.call("watch:button-color", index)
                    }
                }
            }

            Ext.InputButton {
                anchors.horizontalCenter: parent.horizontalCenter
                y: (main.width - height) / 2 + 72
                text: "<"

                background: Rectangle {
                    radius: width / 2
                    color: parent.pressed ? "darkgray" : "lightyellow"
                }
            }

            Row {
                anchors.horizontalCenter: parent.horizontalCenter
                y: (main.width - height) / 2 - 37
                spacing: 17

                Ext.CalcButton { text: "×" }
                Ext.CalcButton { text: "÷" }
            }

            Row {
                anchors.horizontalCenter: parent.horizontalCenter
                y: (main.width - height) / 2 + 37
                spacing: 17

                Ext.CalcButton { text: "+" }
                Ext.CalcButton { text: "-" }
            }
        }

        // extensions (page 3): local and remote IPs

        Rectangle {
            id: ipPage
            color: "#303030"

            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                spacing: 4

                // Swank
                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.weight: Font.DemiBold
                    font.pixelSize: 12
                    color: "lightgray"
                    text: "Swank / local IP"
                }
                RoundButton {
                    anchors.horizontalCenter: parent.horizontalCenter
                    objectName: "swank"
                    font.bold: true
                    text: "Start"

                    onClicked: {
                        text = "Waiting for IP..."
                        ipPage.color = "orange"
                        ipPage.opacity = 0.5
                        Lisp.call("eql:qsleep", 0.1) // for above to be processed instantly
                        Lisp.call("eql:start-swank")
                        ipPage.color = "#303030"
                        ipPage.opacity = 1
                    }
                }

                // spacer
                Item {
                    width: 1
                    height: 5
                }

                // PC (remote) IP
                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.weight: Font.DemiBold
                    font.pixelSize: 12
                    color: "lightgray"
                    text: "PC / remote IP"
                }
                Row {
                    Text {
                        height: ip.height
                        verticalAlignment: Text.AlignVCenter
                        text: "192.168.1."
                        color: "white"
                    }
                    Tumbler {
                        id: ip
                        objectName: "remote_ip"
                        height: 68
                        width: 30
                        model: 256
                        visibleItemCount: 5

                        delegate: Text {
                            height: parent.height
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            opacity: 1.0 - Math.abs(Tumbler.displacement) / (ip.visibleItemCount / 1.7)
                            font.weight: Font.DemiBold
                            color: "white"
                            text: modelData
                        }

                        onCurrentIndexChanged: Lisp.call("watch:save-ip")
                    }
                }
            }
        }
    }

    // input echo

    Text {
        objectName: "echo"
        anchors.centerIn: parent
        font.weight: Font.DemiBold
        color: "white"
        z: 2
        visible: anim.running

        NumberAnimation on font.pixelSize {
            id: anim
            objectName: "echo_anim"
            from: 90
            to: 0
            duration: 400
            easing.type: Easing.InOutSine
        }
    }

    // display

    Rectangle {
        id: display
        anchors.centerIn: parent
        width: main.width - 72
        height: 24
        color: "black"
        radius: 4
        visible: (view.currentIndex !== 2)

        Ext.DisplayText {
            objectName: "float"
            anchors.fill: parent
            text: "0.0"
        }
    }

    // numerator / denominator

    Ext.DisplayText {
        objectName: "real_num"
        anchors.horizontalCenter: main.horizontalCenter
        y: display.y - height - 4
        width: main.width
        color: "lightgray"
        text: "0"
        visible: (view.currentIndex == 0)
    }

    Ext.DisplayText {
        objectName: "real_den"
        anchors.horizontalCenter: main.horizontalCenter
        y: display.y + height
        width: main.width
        color: "lightgray"
        text: "1"
        visible: (view.currentIndex == 0)
    }

    // page indicator

    PageIndicator {
        id: indicator
        anchors.bottom: view.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        height: 20
        count: view.count
        currentIndex: view.currentIndex

        delegate: Rectangle {
            implicitWidth: 8
            implicitHeight: 8
            radius: width / 2
            color: "white"
            opacity: index === indicator.currentIndex ? 1 : 0.35
        }
    }
}
